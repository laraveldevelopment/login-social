<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Stripe, Mailgun, SparkPost and others. This file provides a sane
    | default location for this type of information, allowing packages
    | to have a conventional place to find your various credentials.
    |
    */

    'mailgun' => [
        'domain' => env('MAILGUN_DOMAIN'),
        'secret' => env('MAILGUN_SECRET'),
    ],

    'ses' => [
        'key' => env('SES_KEY'),
        'secret' => env('SES_SECRET'),
        'region' => 'us-east-1',
    ],

    'sparkpost' => [
        'secret' => env('SPARKPOST_SECRET'),
    ],

    'stripe' => [
        'model' => App\User::class,
        'key' => env('STRIPE_KEY'),
        'secret' => env('STRIPE_SECRET'),
    ],

    'github' => [
        'client_id' => 'eab7e06e9cc062e8897b',
        'client_secret' => 'd7d30067d3572f49b483857e39ebdd8fa3bbecdf',
        'redirect' => 'http://localhost:8000/retorno/github',
    ],

    'facebook' => [
        'client_id' => '341037043129455',
        'client_secret' => '8377c326ed0d82d81947e209ec84aaf1',
        'redirect' => 'http://localhost:8000/retorno/facebook',
    ],
];
