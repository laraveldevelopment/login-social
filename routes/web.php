<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index');

Route::get('/entrar/github','SocialAuthController@entrarGithub');
Route::get('/retorno/github','SocialAuthController@retornoGithub');

Route::get('/entrar/facebook','SocialAuthController@entrarFacebook');
Route::get('/retorno/facebook','SocialAuthController@retornoFacebook');
